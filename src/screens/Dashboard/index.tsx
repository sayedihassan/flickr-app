import React, { useCallback, useMemo } from 'react';
import SearchInput from '@components/SearchInput';
import FlickrGallery from '@components/FlickrGallery';
import { debounce, isEmpty } from 'lodash';
import { urlSearchParamBuilder } from '@utils/helpers/url';
import classNames from 'classnames/bind';
import styles from './styles.module.css';
import { Container, Spinners } from '@components/UI';
import { useFetch } from '@services/api';

const cx = classNames.bind(styles);

export default function Dashboard() {
    const { response, isLoading, error, setQuery } = useFetch({ url: "https://www.flickr.com/services/feeds/photos_public.gne?format=json" });
    const fetchFlickrApi = useCallback(
        async (query?: string) => {
            const params = {
                "format": "json",
                ...(!isEmpty(query) && { tags: query })
            }
            const url = urlSearchParamBuilder("https://www.flickr.com/services/feeds/photos_public.gne", params);
            setQuery(url);
        },
        []
    );
    const searchQuery = useMemo(() => debounce(fetchFlickrApi, 300), []);
    return (
        <>
            <div className={cx("search-section")}>
                <div className={cx("search-wrapper")}>
                    <SearchInput
                        label="Search Photos"
                        name="search"
                        onChangeHandler={searchQuery}
                    />
                </div>
            </div>
            <Container>
                {(!isLoading && !error) && <FlickrGallery items={response.items} />}
                {isLoading && <div className={cx("pad-wrapper")}><Spinners.FadingCircle theme="dark" /></div>}
                {(!isLoading && error) && <div className={cx("pad-wrapper")}>Error fetching photos.</div>}
            </Container>
        </>
    )
}