export type FlickrGalleryItems = {
  title: string;
  tags: string;
  author: string;
  date_taken: string;
  link: string;
  media: { m: string };
};
export type FlickrGalleryItem = {
  title: string;
  tags: string;
  author: string;
  date_taken: string;
  link: string;
  media: string;
};

export type FlickrGallery = FlickrGalleryItems[];
