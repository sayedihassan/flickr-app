import React from "react";
import { urlSearchParamBuilder } from "./index";

describe("urlSearchParamBuilder", () => {
  test("should return same url with empty parameters", () => {
    const url = urlSearchParamBuilder("http://test.com", {});
    expect(url).toEqual("http://test.com/");
  });
  test("should return new url with all parameters", () => {
    const url = urlSearchParamBuilder("http://test.com", {
      sort: "asc",
      page: 1,
    });
    expect(url).toEqual("http://test.com/?sort=asc&page=1");

    const testUrl = new URL(url);
    const searchParams = new URLSearchParams(testUrl.search);

    expect(searchParams.has("sort")).toBeTruthy();
    expect(searchParams.has("page")).toBeTruthy();
    expect(searchParams.get("sort")).toEqual("asc");
    expect(searchParams.get("page")).toEqual("1");
  });
  test("should return new url with replaced parameter values", () => {
    const url = urlSearchParamBuilder("http://test.com?sort=desc", {
      sort: "asc",
    });
    expect(url).toEqual("http://test.com/?sort=asc");

    const testUrl = new URL(url);
    const searchParams = new URLSearchParams(testUrl.search);

    expect(searchParams.has("sort")).toBeTruthy();
    expect(searchParams.get("sort")).toEqual("asc");
  });
  test("should return new url with no null or undefined parameter values", () => {
    const url = urlSearchParamBuilder("http://test.com?sort=desc", {
      sort: "asc",
      page: "",
      limit: null,
      skip: undefined,
    });
    expect(url).toEqual("http://test.com/?sort=asc&page=");

    const testUrl = new URL(url);
    const searchParams = new URLSearchParams(testUrl.search);

    expect(searchParams.has("sort")).toBeTruthy();
    expect(searchParams.has("page")).toBeTruthy();

    expect(searchParams.get("sort")).toEqual("asc");
    expect(searchParams.get("page")).toEqual("");

    expect(searchParams.has("limit")).toBeFalsy();
    expect(searchParams.has("skip")).toBeFalsy();
  });
  test("should return new url with no null or undefined parameter values", () => {
    const url = urlSearchParamBuilder("http://test.com?sort=desc", {
      sort: "asc",
      page: "",
      limit: null,
      skip: undefined,
    });
    expect(url).toEqual("http://test.com/?sort=asc&page=");

    const testUrl = new URL(url);
    const searchParams = new URLSearchParams(testUrl.search);

    expect(searchParams.has("sort")).toBeTruthy();
    expect(searchParams.has("page")).toBeTruthy();

    expect(searchParams.get("sort")).toEqual("asc");
    expect(searchParams.get("page")).toEqual("");

    expect(searchParams.has("limit")).toBeFalsy();
    expect(searchParams.has("skip")).toBeFalsy();
  });
});
