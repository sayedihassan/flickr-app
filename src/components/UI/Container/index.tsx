import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.css';

type defaultProps = {
    children: React.ReactNode
}

const cx = classNames.bind(styles);

export default function Container({
    children,
}: defaultProps) {
    return (
        <div className={cx('container')}>
            {children}
        </div>
    )
}
