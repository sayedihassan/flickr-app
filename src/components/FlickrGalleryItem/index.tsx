import React, { useMemo, useCallback } from 'react';
import { FlickrGalleryItem as FlickrGalleryItemProps } from '@appTypes/Flickr';
import { dateTimeFormatter } from '../../utils/helpers/datetime';
import { authorNameSanitizer } from '../../utils/helpers/sanitize';
import classNames from 'classnames/bind';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

/**
 * Flickr Each Gallery Item
 *
 * @param {string} title title of the photo
 * @param {string} tags multiple tags
 * @param {string} author author name
 * @param {string} date_taken date time string of photo taken
 * @param {string} link flickr photo url
 * @param {string} media thumbnail photo
 */
export default function FlickrGalleryItem({
    title, tags, author, date_taken, link, media
}: FlickrGalleryItemProps) {
    const postedOn = dateTimeFormatter(date_taken);
    return (
        <div className={cx('gallery-item')}>
            <a href={link} target="_blank">
                <img alt={title} src={media} width="100%" height="160" />
                <div className={cx('image-overlay')}>View in full &rarr;</div>
            </a>
            <div className={cx('gallery-item-content')}>
                {title && (<h2>{title}</h2>)}
                <div className={cx('author-name')}>By {authorNameSanitizer(author)}{postedOn &&
                    (<span className={cx('date')} data-testid="date"> on {postedOn}</span>)}
                </div>
                {tags && (<div className={cx('tags')} data-testid="tags">Tags: {tags}</div>)}
            </div>
        </div>
    )
}
