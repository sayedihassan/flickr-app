import React from "react";
import { authorNameSanitizer } from "./index";

describe("authorNameSanitizer", () => {
  test("return same name when no brackets", () => {
    const name = authorNameSanitizer("testname");
    expect(name).toEqual("testname");

    const name2 = authorNameSanitizer("");
    expect(name2).toEqual("");
  });
  test("return substring of name after first bracket when only first bracket", () => {
    const name = authorNameSanitizer('test@gmail.com ("testname');
    expect(name).toEqual("testname");

    const name2 = authorNameSanitizer('test@gmail.com (""testname');
    expect(name2).toEqual('"testname');

    const name3 = authorNameSanitizer('test@gmail.com ("');
    expect(name3).toEqual("");
  });
  test("return substring of name before last bracket when only last bracket", () => {
    const name = authorNameSanitizer('test@gmail.com testname")');
    expect(name).toEqual("test@gmail.com testname");

    const name2 = authorNameSanitizer('test@gmail.com testname"")');
    expect(name2).toEqual('test@gmail.com testname"');

    const name3 = authorNameSanitizer('")');
    expect(name3).toEqual("");
  });
});
