import React from 'react';
import classNames from 'classnames/bind';
import FlickrGalleryItem from '@components/FlickrGalleryItem';
import { FlickrGallery } from '@appTypes/Flickr';
import styles from './styles.module.css';


type defaultProps = {
    items: FlickrGallery
}

const cx = classNames.bind(styles);

/**
 * FlickrGallery Grid
 *
 * @param {Array} items list of flickr gallery items
 */
export default function FlickrGallery({
    items,
}: defaultProps) {
    return (
        <div className={cx('gallery-wrapper')}>
            {items.map((item, key) => {
                return (
                    <React.Fragment key={key}>
                        <FlickrGalleryItem
                            title={item.title}
                            tags={item.tags}
                            author={item.author}
                            date_taken={item.date_taken}
                            link={item.link}
                            media={item.media.m}
                        />
                    </React.Fragment>
                )
            })}
        </div>
    )
}
