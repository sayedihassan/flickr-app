import { render, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import FlickrGalleryItem from "./index";

describe("FlickrGalleryItem", () => {
    test("renders SearchInput", () => {
        const wrapper = renderer.create(
            <FlickrGalleryItem
                title={"Test Photo"}
                tags={"birn test"}
                author={'test@gmail.com ("testname")'}
                date_taken={""}
                link={"http://test.com"}
                media={"http://test.com"}
            />
        );
        const tree = wrapper.toJSON()
        expect(tree).toMatchSnapshot();
    });
    test("does not render date when date is invalid or not provided", () => {
        render(<FlickrGalleryItem
            title={"Test Photo"}
            tags={"birn test"}
            author={'test@gmail.com ("testname")'}
            date_taken={""}
            link={"http://test.com"}
            media={"http://test.com"}
        />);
        expect(screen.queryByTestId("date")).toBeNull();
    });
    test("does not render tags when tags is empty or not provided", () => {
        render(<FlickrGalleryItem
            title={"Test Photo"}
            tags={""}
            author={'test@gmail.com ("testname")'}
            date_taken={"2022-06-04T14:21:35-08:00"}
            link={"http://test.com"}
            media={"http://test.com"}
        />);
        expect(screen.queryByTestId("tags")).toBeNull();
    });
})