/**
 * Authorname Sanitizer
 *
 * @param {string} name
 * @returns {string} sanitized name without first instance of (" and last instance of ")
 */
export const authorNameSanitizer = (name: string) => {
  const authorName = name;
  let first = authorName.indexOf('("');
  if (first !== -1) {
    first = first + 2;
  }
  const last = authorName.lastIndexOf('")');
  if (first == -1 && last !== -1) {
    return authorName.slice(0, last);
  } else if (first !== -1 && last == -1) {
    return authorName.slice(first);
  } else if (first == -1 && last == -1) {
    return authorName;
  } else {
    return authorName.slice(first, last);
  }
};
