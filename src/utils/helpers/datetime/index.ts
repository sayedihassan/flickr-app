import moment from "moment";

/**
 * Formats date time string to DD/MM/YYYY
 *
 * @param {string} dateTime
 * @returns {(string|undefined)} Date formatted in DD/MM/YYYY or undefined when empty
 */
export const dateTimeFormatter = (dateTime: string) => {
  const datetime = moment.utc(dateTime);
  if (!datetime.isValid()) {
    return;
  }
  const newDate = datetime.format("DD/MM/YYYY");
  return `${newDate}`;
};
