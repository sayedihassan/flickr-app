import { isEmpty } from "lodash";

interface ParamsProps {
  [key: string]: any;
}
/**
 * Url Search Parameter Builder
 *
 * @param {string} url
 * @param {ParamsProps} params
 * @returns {string}
 */
export const urlSearchParamBuilder = (url: string, params: ParamsProps) => {
  let newUrl = new URL(url);
  if (!isEmpty(params)) {
    for (let k in params) {
      let paramValue = params[k];
      if (paramValue !== undefined && paramValue !== null) {
        newUrl.searchParams.set(k, paramValue);
      }
    }
  }
  return newUrl.href;
};
