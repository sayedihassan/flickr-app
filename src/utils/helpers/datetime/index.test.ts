import React from "react";
import { dateTimeFormatter } from "./index";

describe("dateTimeFormatter", () => {
  test("correct datetime return formmated", () => {
    const datetime = dateTimeFormatter("2022-06-04T15:18:14-08:00");
    expect(datetime).toEqual("04/06/2022");
  });
  test("incorrect datetime return undefined", () => {
    const datetime = dateTimeFormatter("");
    expect(datetime).toBeUndefined();
  });
});
