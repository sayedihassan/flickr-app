import { useEffect, useState } from "react";
import fetchJsonp from "fetch-jsonp";

type defaultFetchProps = {
  url?: string;
};

/**
 * Hook for fetching data
 *
 */
export const useFetch = ({ url = "" }: defaultFetchProps) => {
  const [query, setQuery] = useState<string>(url);
  const [response, setResponse] = useState<any>(null);
  const [error, setError] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  useEffect(() => {
    (async () => {
      if (!query) return;
      try {
        const result = await fetchJsonp(query, {
          jsonpCallbackFunction: "jsonFlickrFeed",
        });
        const data = await result.json();
        setError(false);
        setResponse(data);
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        setError(true);
      }
    })();
  }, [query]);
  return { response, error, isLoading, setQuery };
};
