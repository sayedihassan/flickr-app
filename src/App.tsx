import React from 'react';
import { Routes, Route } from "react-router-dom";
import './App.css';

import Dashboard from "@screens/Dashboard";

function App() {
  return (
    <Routes>
      <Route
        path="/"
        element={<Dashboard />}
      />
    </Routes>
  );
}

export default App;
