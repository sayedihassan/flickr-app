import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.module.css';

type defaultProps = {
    label: string;
    name: string;
    placeholder?: string;
    defaultValue?: string;
    onChangeHandler: (val: string) => void;
}

const cx = classNames.bind(styles);

/**
 * Search Input
 *
 * @param {string} label title text for the input
 * @param {string} name field name
 * @param {string} placeholder placeholder text
 * @param {string} defaultValue default value
 * @param {(val: string) => void} onChangeHandler - input text handler callback
 */
export default function SearchInput({
    label,
    name,
    placeholder = "",
    defaultValue = "",
    onChangeHandler
}: defaultProps) {
    return (
        <>
            <label htmlFor={`${name}-bar`} className={cx('label')}>{label}</label>
            <input
                id={`${name}-bar`}
                className={cx('input-text')}
                type="text"
                name={name}
                placeholder={placeholder}
                defaultValue={defaultValue}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeHandler(e.target.value)}
            />
        </>
    )
}