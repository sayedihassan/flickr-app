import React from "react";
import SearchInput from "./index";
import renderer from "react-test-renderer";
import { render, screen, fireEvent } from '@testing-library/react';

describe("SearchInput", () => {
  test("renders SearchInput", () => {
    const wrapper = renderer.create(
      <SearchInput
        label="Search Photos"
        name="search"
        onChangeHandler={() => { }}
      />
    );
    const tree = wrapper.toJSON()
    expect(tree).toMatchSnapshot();
  });
  test("calls onChangeHandler when SearchInput value changes", () => {
    const handleChange = jest.fn();
    render(
      <SearchInput
        label="Search Photos"
        name="search"
        onChangeHandler={handleChange}
      />
    );
    const inputNode: HTMLInputElement = screen.getByLabelText('Search Photos', { selector: 'input' });
    fireEvent.change(inputNode, { target: { value: 'bird' } });
    expect(inputNode.value).toBe('bird');
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith('bird');


    fireEvent.change(inputNode, { target: { value: '' } });
    expect(inputNode.value).toBe('');
    expect(handleChange).toHaveBeenCalledTimes(2);
    expect(handleChange).toHaveBeenCalledWith('');
  });
});
